﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeginnenMetCSharp
{
    class HoofdstukVier
    {
        static public void BMIBerekenaar()
        {
            Console.WriteLine("Hoeveel weeg je in kg ?");
            double gewicht = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Hoe groot ben je in m?");
            double grote = Convert.ToDouble(Console.ReadLine());
            double bmi = gewicht/Math.Pow(grote, 2);
            Console.WriteLine($"Je BMI bedraagt {bmi:F2}.");
        }
        static public void Pythagoras()
        {
            Console.WriteLine("Geef de lengte van de eerste rechthoekszijde:");
            double eerste = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Geef de lengte van de tweede rechthoekszijde:");
            double tweede = Convert.ToDouble(Console.ReadLine());
            double schuine = Math.Sqrt(Math.Pow(eerste, 2) + Math.Pow(tweede, 2));
            Console.WriteLine($"De lengte van de schuine zijde is {schuine:F2}");
        }
        static public void Cikels()
        {
            Console.WriteLine("Geef de straal:");
            double straal = Convert.ToDouble(Console.ReadLine());
            double omtrek = 2*Math.PI*straal;
            double oppervlakte = Math.Pow(straal,2) *Math.PI;
            Console.WriteLine($"De omtrek van een cirkel met straal {straal} is {omtrek:F2}.");
            Console.WriteLine($"De oppervlakte is {oppervlakte:f2}.");
        }
        static public void Orakeltje()
        {
            Console.WriteLine("Hoe oud ben je nu?");
            int leeftijd = Convert.ToInt32(Console.ReadLine());
            Random mygen = new Random();
            int tijd = mygen.Next(20, 125);
            Console.WriteLine($"Je zal nog {tijd} jaar leven. Je zal dus {tijd + leeftijd} worden.");
        }
    }
}
