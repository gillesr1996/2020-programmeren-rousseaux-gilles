﻿using System;

namespace BeginnenMetCSharp
{
    class EenProgrammaSchrijvenInCSharp
    {
        static public void ZegGoedendag()
        {
            Console.Write("Hoe heet je? ");
            string invoer = Console.ReadLine();
            Console.Write(invoer);
            Console.WriteLine(", ik wens je een goedendag!");
        }
        static public void GekleurdeRommelzin()
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("Wat is je favoriete kleur?");
            Console.ResetColor();
            string kleur = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("Wat is je favoriete eten?");
            Console.ResetColor();
            string eten = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine("Wat is je favoriete auto?");
            Console.ResetColor();
            string auto = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Wat is je favoriete film?");
            Console.ResetColor();
            string film = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Wat is je favoriete boek?");
            Console.ResetColor();
            string boek = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("11 Je favoriete kleur is " + eten + ". Je eet graag " + auto + ". Je lievelingsfilm is " + boek + ". Je beste boek is " + kleur + ".");
            Console.ResetColor();
        }
        static public void SayHello()
        {
            //Test

            //Input/Output: ReadLine/WriteLine
            Console.WriteLine("Hoi, ik ben het!");
            Console.WriteLine("Wie ben jij?!!");
            string result;
            result = Console.ReadLine();
            Console.Write("Dag ");
            Console.Write(result);
            Console.WriteLine(" hoe gaat het met je?");
            Console.WriteLine("Dag " + result + " hoe gaat het met je?");

            string leeftijd;
            string adres;
            Console.WriteLine("Geef leeftijd");
            leeftijd = Console.ReadLine();
            Console.WriteLine("Geef adres");
            adres = Console.ReadLine();

            //Kleuren in Console
            Console.BackgroundColor = ConsoleColor.DarkCyan;
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.ResetColor();
            Console.WriteLine("Tekst in de standaard kleur");
            Console.BackgroundColor = ConsoleColor.Blue;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Deze tekst komt in het groen met blauwe achtergrond");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("En deze in het rood met blauwe achtergrond");
            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Error!!!!! Contacteer de helpdesk");
            Console.ResetColor();
            Console.WriteLine("Het programma sluit nu af");

            //Mogelijke kleuren
            Console.ForegroundColor = ConsoleColor.DarkBlue;
            Console.WriteLine("Foreground color is set to DarkBlue");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("Foreground color is set to DarkGreen");
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("Foreground color is set to DarkCyan");
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("Foreground color is set to DarkRed");
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            Console.WriteLine("Foreground color is set to DarkMagenta");
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine("Foreground color is set to DarkYellow");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("Foreground color is set to Gray");
            Console.ForegroundColor = ConsoleColor.DarkGray;
            Console.WriteLine("Foreground color is set to DarkGrey");
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine("Foreground color is set to DarkYellow");
            Console.WriteLine("=======================================");
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine("Foreground color is set to Black");
        }
    }
}
