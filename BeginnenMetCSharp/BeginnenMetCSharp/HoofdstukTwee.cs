﻿using System;

namespace BeginnenMetCSharp
{
    class HoofdstukTwee
    {
        static public void Optellen()
        {
            Console.WriteLine("Wat is het eerste getal?");
            int eerste = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Wat is het tweede getal?");
            int tweede = Convert.ToInt32(Console.ReadLine());
            
            int som = eerste + tweede;
            Console.WriteLine("De som is "+som+".");
        }
        static public void VerbruikWagen()
        {
            double Lvoor;
            double Lna;
            double Kmvoor;
            double Kmna;
            Console.Write("Geef het aantal liter in de tank voor de rit: ");
            Lvoor = Convert.ToInt32(Console.ReadLine());
            Console.Write("Geef het aantal liter in de tank na de rit: ");
            Lna = Convert.ToInt32(Console.ReadLine());
            Console.Write("Geef de kilometerstand van je auto voor de rit: ");
            Kmvoor = Convert.ToInt32(Console.ReadLine());
            Console.Write("Geef de kilometerstand van je auto na de rit: ");
            Kmna = Convert.ToInt32(Console.ReadLine());
            Console.Write("Het verbruik van de auto is: "+(100 * (Lvoor - Lna) / (Kmna - Kmvoor)));
        }
        static public void BeetjeWiskunde()
        {
            Console.WriteLine(-1 + 4 * 6);
            Console.WriteLine((35 + 5) % 7);
            Console.WriteLine(14 + -4 * 6 / 11);
            Console.WriteLine(2 + 15 / 6 * 1 - 7 % 2);
        }
        static public void Gemiddelde()
        {
            Console.WriteLine((18+11+8)/3);
        }
        static public void Maaltafels()
        {
            int tafel = 411;
            Console.Write("1 * " + tafel + " is " + (1 * tafel) + ".");
            Console.ReadLine();
            Console.Clear();
            Console.Write("2 * " + tafel + " is " + (2 * tafel) + ".");
            Console.ReadLine();
            Console.Clear();
            Console.Write("3 * " + tafel + " is " + (3 * tafel) + ".");
            Console.ReadLine();
            Console.Clear();
            Console.Write("4 * " + tafel + " is " + (4 * tafel) + ".");
            Console.ReadLine();
            Console.Clear();
            Console.Write("5 * " + tafel + " is " + (5 * tafel) + ".");
            Console.ReadLine();
            Console.Clear();
            Console.Write("6 * " + tafel + " is " + (6 * tafel) + ".");
            Console.ReadLine();
            Console.Clear();
            Console.Write("7 * " + tafel + " is " + (7 * tafel) + ".");
            Console.ReadLine();
            Console.Clear();
            Console.Write("8 * " + tafel + " is " + (8 * tafel) + ".");
            Console.ReadLine();
            Console.Clear();
            Console.Write("9 * " + tafel + " is " + (9 * tafel) + ".");
            Console.ReadLine();
            Console.Clear();
            Console.Write("10 * " + tafel + " is " + (10 * tafel) + ".");
            Console.ReadLine();
        }
        static public void Ruimte()
        {
            int gewicht;
            double Mercurius = 0.38;
            double Venus = 0.91;
            double Aarde = 1.00;
            double Mars = 0.38;
            double Jupiter = 2.34;
            double Saturnus = 1.06;
            double Uranus = 0.92;
            double Neptunus  =1.19;
            double Pluto = 0.06;
            Console.WriteLine("Geef je gewicht in kg.");
            gewicht = Convert.ToInt32(Console.ReadLine());
            Console.Clear();
            Console.WriteLine("op Mercurius voel je je alsof je "+gewicht*Mercurius+"kg weegt.");
            Console.WriteLine("op Venus voel je je alsof je " + gewicht * Venus + "kg weegt.");
            Console.WriteLine("op Aarde voel je je alsof je " + gewicht * Aarde + "kg weegt.");
            Console.WriteLine("op Mars voel je je alsof je " + gewicht * Mars + "kg weegt.");
            Console.WriteLine("op Jupiter voel je je alsof je " + gewicht * Jupiter + "kg weegt.");
            Console.WriteLine("op Saturnus voel je je alsof je " + gewicht * Saturnus + "kg weegt.");
            Console.WriteLine("op Uranus voel je je alsof je " + gewicht * Uranus + "kg weegt.");
            Console.WriteLine("op Neptunus voel je je alsof je " + gewicht * Neptunus + "kg weegt.");
            Console.WriteLine("op Pluto voel je je alsof je " + gewicht * Pluto + "kg weegt.");
        }
    }
}
