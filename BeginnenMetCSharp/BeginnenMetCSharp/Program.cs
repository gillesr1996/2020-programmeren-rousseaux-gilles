﻿using System;

namespace BeginnenMetCSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            Hoofdmenu();
        }
        static void Hoofdmenu()
        {
            Console.WriteLine($"Kies uit volgende klassen door een cijfer te tikken:\n" +
                $"\t0 - APDotCom\n" +
                $"\t1 - HoodstukEen\n" +
                $"\t2 - HoofdstukTwee\n" +
                $"\t3 - HoofdstukDrie\n" +
                $"\t4 - HoofdstukVier\n" +
                $"\t5 - HoofdstukVijf\n" +
                $"\t6 - HoofdstukZes\n" +
                $"\t7 - HoofdstukZeven\n");
            int hoofdstuk = Convert.ToInt32(Console.ReadLine());
            Console.Clear();
            if (hoofdstuk < 0 || hoofdstuk > 7)
            {
                Hoofdmenu();
            }
            if (hoofdstuk == 0)
            {
                Console.WriteLine($"Kies uit volgende methodes door een cijfer te tikken:\n" +
                    $"\t1 – Bestel\n" +
                    $"\t2 – BestelMetVraagEnAanbod\n" +
                    $"\t3 – BestelConditioneel\n");
                int methode = Convert.ToInt32(Console.ReadLine());
                Console.Clear();
                if (methode < 1 || methode > 3)
                {
                    Hoofdmenu();
                }
                if (methode == 1)
                {
                    APDotCom.Bestel();
                }
                if (methode == 2)
                {
                    APDotCom.BestelMetVraagEnAanbod();
                }
                if (methode == 3)
                {
                    APDotCom.BestelConditioneel();
                }
            }
            if (hoofdstuk == 1)
            {
                Console.WriteLine($"Kies uit volgende methodes door een cijfer te tikken:\n" +
                    $"\t1 – ZegGoedendag\n" +
                    $"\t2 – GekleurdeRommelzin\n" +
                    $"\t3 – SayHello\n");
                int methode = Convert.ToInt32(Console.ReadLine());
                Console.Clear();
                if (methode < 1 || methode > 3)
                {
                    Hoofdmenu();
                }
                if (methode == 1)
                {
                    EenProgrammaSchrijvenInCSharp.ZegGoedendag();
                }
                if (methode == 2)
                {
                    EenProgrammaSchrijvenInCSharp.GekleurdeRommelzin();
                }
                if (methode == 3)
                {
                    EenProgrammaSchrijvenInCSharp.SayHello();
                }
            }
            if (hoofdstuk == 2)
            {
                Console.WriteLine($"Kies uit volgende methodes door een cijfer te tikken:\n" +
                    $"\t1 – Optellen\n" +
                    $"\t2 – VerbruikWagen\n" +
                    $"\t3 – BeetjeWiskunde\n" +
                    $"\t4 – Gemiddelde\n" +
                    $"\t5 – Maaltafels\n" +
                    $"\t6 – Ruimte\n");
                int methode = Convert.ToInt32(Console.ReadLine());
                Console.Clear();
                if (methode < 1 || methode > 6)
                {
                    Hoofdmenu();
                }
                if (methode == 1)
                {
                    HoofdstukTwee.Optellen();
                }
                if (methode == 2)
                {
                    HoofdstukTwee.VerbruikWagen();
                }
                if (methode == 3)
                {
                    HoofdstukTwee.BeetjeWiskunde();
                }
                if (methode == 4)
                {
                    HoofdstukTwee.Gemiddelde();
                }
                if (methode == 5)
                {
                    HoofdstukTwee.Maaltafels();
                }
                if (methode == 6)
                {
                    HoofdstukTwee.Ruimte();
                }
            }
            if (hoofdstuk == 3)
            {
                Console.WriteLine($"Kies uit volgende methodes door een cijfer te tikken:\n" +
                    $"\t1 – VariabelenHoofdletters\n" +
                    $"\t2 – StringInterpolation\n" +
                    $"\t3 – BerekenBtw\n" +
                    $"\t4 – LeetSpeak\n" +
                    $"\t5 – Instructies\n" +
                    $"\t6 – Lotto\n" +
                    $"\t7 – Superlotto\n");
                int methode = Convert.ToInt32(Console.ReadLine());
                Console.Clear();
                if (methode < 1 || methode > 7)
                {
                    Hoofdmenu();
                }
                if (methode == 1)
                {
                    HoofdstukDrie.VariabelenHoofdletters();
                }
                if (methode == 2)
                {
                    HoofdstukDrie.StringInterpolation();
                }
                if (methode == 3)
                {
                    HoofdstukDrie.BerekenBtw();
                }
                if (methode == 4)
                {
                    HoofdstukDrie.LeetSpeak();
                }
                if (methode == 5)
                {
                    HoofdstukDrie.Instructies();
                }
                if (methode == 6)
                {
                    HoofdstukDrie.Lotto();
                }
                if (methode == 7)
                {
                    HoofdstukDrie.Superlotto();
                }
            }
            if (hoofdstuk == 4)
            {
                Console.WriteLine($"Kies uit volgende methodes door een cijfer te tikken:\n" +
                    $"\t1 – BMIBerekenaar\n" +
                    $"\t2 – Pythagoras\n" +
                    $"\t3 – Cikels\n" +
                    $"\t4 – LeetSpeak\n");
                int methode = Convert.ToInt32(Console.ReadLine());
                Console.Clear();
                if (methode < 1 || methode > 4)
                {
                    Hoofdmenu();
                }
                if (methode == 1)
                {
                    HoofdstukVier.BMIBerekenaar();
                }
                if (methode == 2)
                {
                    HoofdstukVier.Pythagoras();
                }
                if (methode == 3)
                {
                    HoofdstukVier.Cikels();
                }
                if (methode == 4)
                {
                    HoofdstukVier.Orakeltje();
                }
            }
            if (hoofdstuk == 5)
            {
                Console.WriteLine($"Kies uit volgende methodes door een cijfer te tikken:\n" +
                    $"\t1 – BMIBerkenaar\n" +
                    $"\t2 – Schoenverkoper\n" +
                    $"\t3 – WetVanOhm\n" +
                    $"\t4 – Schrikkeljaar\n" +
                    $"\t5 – SimpeleRekenmachine\n");
                int methode = Convert.ToInt32(Console.ReadLine());
                Console.Clear();
                if (methode < 1 || methode > 5)
                {
                    Hoofdmenu();
                }
                if (methode == 1)
                {
                    HoofdstukVijf.BMIBerkenaar();
                }
                if (methode == 2)
                {
                    HoofdstukVijf.Schoenverkoper();
                }
                if (methode == 3)
                {
                    HoofdstukVijf.WetVanOhm();
                }
                if (methode == 4)
                {
                    HoofdstukVijf.Schrikkeljaar();
                }
                if (methode == 5)
                {
                    HoofdstukVijf.SimpeleRekenmachine();
                }
            }
            if (hoofdstuk == 6)
            {
                Console.WriteLine($"Kies uit volgende methodes door een cijfer te tikken:\n" +
                    $"\t1 – BasisEen\n" +
                    $"\t2 – BasisTwee\n" +
                    $"\t3 – BasisDrie\n" +
                    $"\t4 – BasisVier\n" +
                    $"\t5 – BasisVijf\n" +
                    $"\t6 – BasisZes\n" +
                    $"\t7 - Maaltafels\n" +
                    $"\t8 - RNA");
                int methode = Convert.ToInt32(Console.ReadLine());
                Console.Clear();
                if (methode < 1 || methode > 8)
                {
                    Hoofdmenu();
                }
                if (methode == 1)
                {
                    HoofdstukZes.BasisEen();
                }
                if (methode == 2)
                {
                    HoofdstukZes.BasisTwee();
                }
                if (methode == 3)
                {
                    HoofdstukZes.BasisDrie();
                }
                if (methode == 4)
                {
                    HoofdstukZes.BasisVier();
                }
                if (methode == 5)
                {
                    HoofdstukZes.BasisVijf();
                }
                if (methode == 6)
                {
                    HoofdstukZes.BasisZes();
                }
                if (methode == 7)
                {
                    HoofdstukZes.Maaltafels();
                }
                if (methode == 8)
                {
                    HoofdstukZes.RNA();
                }
            }

            if (hoofdstuk == 6)
            {
                Console.WriteLine($"Kies uit volgende methodes door een cijfer te tikken:\n" +
                    $"\t1 – BasisEen\n" +
                    $"\t2 – BasisTwee\n" +
                    $"\t3 – BasisDrie\n" +
                    $"\t4 – BasisVier\n" +
                    $"\t5 – BasisVijf\n" +
                    $"\t6 – BasisZes\n" +
                    $"\t7 - Maaltafels\n" +
                    $"\t8 - RNA");
                int methode = Convert.ToInt32(Console.ReadLine());
                Console.Clear();
                if (methode < 1 || methode > 8)
                {
                    Hoofdmenu();
                }
                if (methode == 1)
                {
                    HoofdstukZes.BasisEen();
                }
            }
        }
    }
}
