﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeginnenMetCSharp
{
    class HoofdstukZes
    {
        static public void BasisEen()
        {
            int getal = 1;
            while (getal <= 100)
            {
                Console.WriteLine(getal);
                getal++;
            }
        }
        static public void BasisTwee()
        {
            int getal = -100;
            while (getal <= 100)
            {
                Console.WriteLine(getal);
                getal++;
            }
        }
        static public void BasisDrie()
        {
            for (int g=1; g<=100; g++)
            {
                Console.WriteLine(g);
            }
        }
        static public void BasisVier()
        {
            for (int g = -100; g <= 100; g++)
            {
                Console.WriteLine(g);
            }
        }
        static public void BasisVijf()
        {
            int g = 1;
            while (g<=100) 
            {   
                if (g%6==0 || g%8==0)
                { 
                    Console.WriteLine(g);
                }
                g++;
            }
        }
        static public void BasisZes()
        {
            for (int g = 1; g <= 100; g++)
            {
                if (g % 6 == 0 || g % 8 == 0)
                {
                    Console.WriteLine(g);
                }
            }
        }
        static public void Maaltafels()
        {
            int maal = 1;
            for (int tafel = 411; maal <= 10 ;maal++)
            {
                Console.WriteLine($"{maal} * {tafel} = {maal*tafel}.");
            }
        }
        static public void RNA()
        {
            string RNA = "";
            Console.WriteLine("Geef nucleotiden:");
            string invoer = "";
            do
            {
                invoer = Console.ReadLine();
                switch (invoer)
                {
                    case "G":
                        RNA += "C";
                        break;
                    case "C":
                        RNA += "G";
                        break;
                    case "T":
                        RNA += "A";
                        break;
                    case "A":
                        RNA += "U";
                        break;
                }
            } while (!string.IsNullOrEmpty(invoer));
            Console.WriteLine($"Je resultaat is: {RNA}");
        }
        static public void OpwarmerPlusEen()
        {
            
        }
    }
}
