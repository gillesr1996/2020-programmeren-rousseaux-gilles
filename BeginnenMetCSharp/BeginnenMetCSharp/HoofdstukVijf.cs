﻿using System;

namespace BeginnenMetCSharp
{
    class HoofdstukVijf
    {
        static public void BMIBerkenaar()
        {
            Console.WriteLine("Hoeveel weeg je in kg ?");
            double gewicht = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Hoe groot ben je in m?");
            double grote = Convert.ToDouble(Console.ReadLine());
            double bmi = gewicht / Math.Pow(grote, 2);
            Console.WriteLine($"Je BMI bedraagt {bmi:F2}.");
            if (bmi < 18.5)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("ondergewicht");
            }
            if ((bmi >= 18.5) && (bmi < 25.0))
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("normaal gewicht");
            }
            
            if ((bmi < 30.0) && (bmi >= 25.0))
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("overgewicht");
            }
            if ((bmi >= 30.0) && (bmi < 40.0))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("zwaarlijvig");
            }
            if ( bmi >= 40.0)
            {
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.WriteLine("ernstige obesitas");
            }
            Console.ResetColor();
        }
        static public void Schoenverkoper()
        {
            Console.WriteLine("Vanaf welk aantal geldt de korting?");
            int aantalkorting = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Hoeveel paar schoenen wil je kopen?");
            int aantal = Convert.ToInt32(Console.ReadLine());
            if (aantal >= aantalkorting)
            {
                int prijs = 10;
                Console.WriteLine($"Je moet {prijs * aantal} euro betalen.");
            }
            else
            {
                int prijs = 20;
                Console.WriteLine($"Je moet {prijs * aantal} euro betalen.");
            }
        }
        static public void WetVanOhm()
        {
            Console.WriteLine($"Wat wil je berekenen? spanning, weerstand of stroomsterkte?");
            string keuze = Console.ReadLine();
            if (keuze == "spanning")
            {
                Console.WriteLine("Wat is de weerstand?");
                double weerstand = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Wat is de stroomsterkte?");
                double stroomsterkte = Convert.ToInt32(Console.ReadLine());
                double spanning = weerstand * stroomsterkte;
                Console.WriteLine($"De spanning bedraagt {spanning}.");
            }
            if (keuze == "weerstand")
            {
                Console.WriteLine("Wat is de spanning?");
                double spanning = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Wat is de stroomsterkte?");
                double stroomsterkte = Convert.ToInt32(Console.ReadLine());
                double weerstand = spanning /stroomsterkte;
                Console.WriteLine($"De weerstand  bedraagt {weerstand }.");
            }
            if (keuze == "stroomsterkte")
            {
                Console.WriteLine("Wat is de weerstand?");
                double weerstand = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Wat is de spanning?");
                double spanning = Convert.ToInt32(Console.ReadLine());
                double stroomsterkte= spanning/ weerstand ;
                Console.WriteLine($"De stroomsterkte bedraagt {stroomsterkte}.");
            }
        }
        static public void Schrikkeljaar()
        {
            int jaar = Convert.ToInt32(Console.ReadLine());
            if (0==jaar%4 && 0 != jaar % 100 || 0 == jaar % 400)
            {
                Console.WriteLine("schrikkeljaar");
            }
            else
            {
                Console.WriteLine("geen schrikkeljaar");
            }
        }
        static public void SimpeleRekenmachine()
        {
            Console.WriteLine("Geef een eerste getal in:");
            double eerste = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Geef een tweede getal in:");
            double tweede = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Welke berekening wil je maken? (+,-,*,/)");
            string berekening = Console.ReadLine();
            if (berekening == "+" ) 
            {
                Console.WriteLine($"{eerste+tweede}");
            }
            if (berekening == "-")
            {
                Console.WriteLine($"{eerste-tweede}");
            }
            if (berekening == "*")
            {
                Console.WriteLine($"{eerste*tweede}");
            }
            if (berekening == "/")
            {
                Console.WriteLine($"{eerste/tweede}");
            }
        }
    }
}
