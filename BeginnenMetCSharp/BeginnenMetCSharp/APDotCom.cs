﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeginnenMetCSharp
{
    class APDotCom
    {
        static public void Bestel()
        {
            Console.WriteLine("Prijs van een boek?");
            double boek = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Prijs van een CD?");
            double cd = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Prijs van een service?");
            double service = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Prijs van een springkasteel?");
            double springkasteel = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Aantal boeken?");
            int aantalboek = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Aantal CD's?");
            int aantalcd = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Aantal serviezen?");
            int aantalservice = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Aantal sprinkastelen?");
            int aantalspringkasteel = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Percentage korting?");
            int korting = Convert.ToInt32(Console.ReadLine());
            double totaalvoor = ((aantalboek * boek) + (aantalcd * cd) + (aantalservice * service) + (aantalspringkasteel * springkasteel));
            double totaalna = totaalvoor *(1.0- korting / 100.0);
            Console.WriteLine($"Uw kasticket \n------------\nboek x {aantalboek}: {boek*aantalboek:f2}\nCD x {aantalcd}: {cd * aantalcd:f2}\nservices x {aantalservice}: { service* aantalservice:f2}\nspringkasteel x {aantalspringkasteel}: {springkasteel * aantalspringkasteel:f2}\nKORTING: {korting}%\nTOTAAL VOOR KORTING: {totaalvoor:F2}\nTOTAAL: {totaalna:F2}");

        }
        static public void BestelMetVraagEnAanbod()
        {
            Console.WriteLine("Basisrijs van een boek?");
            double boek = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Basisrijs van een CD?");
            double cd = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Basisrijs van een service?");
            double service = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Basisrijs van een springkasteel?");
            double springkasteel = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Aantal boeken?");
            int aantalboek = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Aantal CD's?");
            int aantalcd = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Aantal serviezen?");
            int aantalservice = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Aantal sprinkastelen?");
            int aantalspringkasteel = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Percentage korting?");
            int korting = Convert.ToInt32(Console.ReadLine());

            Random mygen = new Random();
            int veaboek = mygen.Next(-50, 50);
            int veacd = mygen.Next(-50, 50);
            int veaservice = mygen.Next(-50, 50);
            int veaspringkateel = mygen.Next(-50, 50);

            double totaalboek = boek * aantalboek * (1.0+veaboek/100.0);
            double totaalcd= cd * aantalcd * (1.0 + veacd / 100.0);
            double totaalservice= service * aantalservice * (1.0 + veaservice / 100.0);
            double totaalsprinkasteel= springkasteel * aantalspringkasteel * (1.0 + veaspringkateel / 100.0);

            double totaalvoor = totaalboek+totaalcd+totaalservice+totaalsprinkasteel;
            double totaalna = totaalvoor * (1.0 - korting / 100.0);
            Console.WriteLine($"Uw kasticket \n------------\n" +
                $"vraag en aanbod boeken: {veaboek}%\nvraag en aanbod CD's: {veacd}%\nvraag en aanbod serviezen: {veaservice}%\nvraag en aanbod springkastelen: {veaspringkateel}%\n" +
                $"boek x {aantalboek}: {totaalboek:f2}\nCD x {aantalcd}: {totaalcd:f2}\nservices x {aantalservice}: { totaalservice:f2}\nspringkasteel x {aantalspringkasteel}: {totaalsprinkasteel:f2}\n" +
                $"KORTING: {korting}%\nTOTAAL VOOR KORTING: {totaalvoor:F2}\nTOTAAL: {totaalna:F2}");

        }
        static public void BestelConditioneel()
        {
            Console.WriteLine("Basisrijs van een boek?");
            double boek = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Basisrijs van een CD?");
            double cd = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Basisrijs van een service?");
            double service = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Basisrijs van een springkasteel?");
            double springkasteel = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Aantal boeken?");
            int aantalboek = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Aantal CD's?");
            int aantalcd = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Aantal serviezen?");
            int aantalservice = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Aantal sprinkastelen?");
            int aantalspringkasteel = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Percentage korting?");
            int korting = Convert.ToInt32(Console.ReadLine());
            if (korting > 30) 
            {
                Console.WriteLine("Waarschuwing: het ingevoerde percentage is hoog!\n");
            }
            Console.WriteLine("Worden prijsstijgingen en -dalingen toegepast? (J/N)");
            string toepassing = Console.ReadLine().ToUpper();
            if (toepassing == "J")
            {
                Random mygen = new Random();
                int veaboek = mygen.Next(-50, 50);
                int veacd = mygen.Next(-50, 50);
                int veaservice = mygen.Next(-50, 50);
                int veaspringkateel = mygen.Next(-50, 50);

                double totaalboek = boek * aantalboek * (1.0 + veaboek / 100.0);
                double totaalcd = cd * aantalcd * (1.0 + veacd / 100.0);
                double totaalservice = service * aantalservice * (1.0 + veaservice / 100.0);
                double totaalsprinkasteel = springkasteel * aantalspringkasteel * (1.0 + veaspringkateel / 100.0);
                double totaalvoor = totaalboek + totaalcd + totaalservice + totaalsprinkasteel;
                double totaalna = totaalvoor * (1.0 - korting / 100.0);
                
                Console.Write($"\nUw kasticket \n------------\n" +
                    $"vraag en aanbod boeken: ");
                if (veaboek >0)
                {Console.ForegroundColor = ConsoleColor.Red; }
                else
                {Console.ForegroundColor = ConsoleColor.Green;}
                Console.Write(veaboek);
                Console.ResetColor();
                Console.Write($"% \nvraag en aanbod CD's: ");
                if (veacd > 0)
                { Console.ForegroundColor = ConsoleColor.Red; }
                else
                { Console.ForegroundColor = ConsoleColor.Green; }
                Console.Write(veacd);
                Console.ResetColor();
                Console.Write($"% \nvraag en aanbod serviezen: ");
                if (veaservice > 0)
                { Console.ForegroundColor = ConsoleColor.Red; }
                else
                { Console.ForegroundColor = ConsoleColor.Green; }
                Console.Write(veaservice);
                Console.ResetColor();
                Console.Write($"% \nvraag en aanbod springkastelen: ");
                if (veaspringkateel > 0)
                { Console.ForegroundColor = ConsoleColor.Red; }
                else
                { Console.ForegroundColor = ConsoleColor.Green; }
                Console.Write(veaspringkateel);
                Console.ResetColor();
                Console.WriteLine($"% \n" +
                    $"boek x {aantalboek}: {totaalboek:f2}\n" +
                    $"CD x {aantalcd}: {totaalcd:f2}\n" +
                    $"services x {aantalservice}: { totaalservice:f2}\n" +
                    $"springkasteel x {aantalspringkasteel}: {totaalsprinkasteel:f2}\n" +
                    $"KORTING: {korting}%\n" +
                    $"TOTAAL VOOR KORTING: {totaalvoor:F2}\n" +
                    $"TOTAAL: {totaalna:F2}");
            }
            if (toepassing == "N")
            {
                double totaalboek = boek * aantalboek;
                double totaalcd = cd * aantalcd;
                double totaalservice = service;
                double totaalsprinkasteel = springkasteel * aantalspringkasteel;
                double totaalvoor = totaalboek + totaalcd + totaalservice + totaalsprinkasteel;
                double totaalna = totaalvoor * (1.0 - korting / 100.0);

                Console.WriteLine($"\nUw kasticket \n------------\n" +
                    $"boek x {aantalboek}: {totaalboek:f2}\n" +
                    $"CD x {aantalcd}: {totaalcd:f2}\n" +
                    $"services x {aantalservice}: { totaalservice:f2}\n" +
                    $"springkasteel x {aantalspringkasteel}: {totaalsprinkasteel:f2}\n" +
                    $"KORTING: {korting}%\n" +
                    $"TOTAAL VOOR KORTING: {totaalvoor:F2}\n" +
                    $"TOTAAL: {totaalna:F2}");
            }
        }
    }
}
