﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeginnenMetCSharp
{
    class HoofdstukDrie
    {
        static public void VariabelenHoofdletters()
        {
            Console.WriteLine("Welke tekst moet ik omzetten?");
            string invoer = Console.ReadLine();
            Console.WriteLine(invoer.ToUpper());
        }
        static public void StringInterpolation()
        {
            int tafel = 411;
            Console.Write($"1 * {tafel} is {(1 * tafel)}.");
            Console.ReadLine();
            Console.Clear();
            Console.Write($"2 * {tafel} is {(2 * tafel)}.");
            Console.ReadLine();
            Console.Clear();
            Console.Write($"3 * {tafel} is {(3 * tafel)}.");
            Console.ReadLine();
            Console.Clear();
            Console.Write($"4 * {tafel} is {(4 * tafel)}.");
            Console.ReadLine();
            Console.Clear();
            Console.Write($"5 * {tafel} is {(5 * tafel)}.");
            Console.ReadLine();
            Console.Clear();
            Console.Write($"6 * {tafel} is {(6 * tafel)}.");
            Console.ReadLine();
            Console.Clear();
            Console.Write($"7 * {tafel} is {(7 * tafel)}.");
            Console.ReadLine();
            Console.Clear();
            Console.Write($"8 * {tafel} is {(8 * tafel)}.");
            Console.ReadLine();
            Console.Clear();
            Console.Write($"9 * {tafel} is {(9 * tafel)}.");
            Console.ReadLine();
            Console.Clear();
            Console.Write($"10 * {tafel} is {(10 * tafel)}.");
            Console.ReadLine();

            Console.Clear();

            int gewicht;
            double Mercurius = 0.38;
            double Venus = 0.91;
            double Aarde = 1.00;
            double Mars = 0.38;
            double Jupiter = 2.34;
            double Saturnus = 1.06;
            double Uranus = 0.92;
            double Neptunus = 1.19;
            double Pluto = 0.06;
            Console.WriteLine("Geef je gewicht in kg.");
            gewicht = Convert.ToInt32(Console.ReadLine());
            Console.Clear();
            Console.WriteLine($"op Mercurius voel je je alsof je {gewicht * Mercurius:F2}kg weegt.");
            Console.WriteLine($"op Venus voel je je alsof je {gewicht * Venus:F2}kg weegt.");
            Console.WriteLine($"op Aarde voel je je alsof je {gewicht* Aarde:F2}kg weegt.");
            Console.WriteLine($"op Mars voel je je alsof je {gewicht * Mars:F2}kg weegt.");
            Console.WriteLine($"op Jupiter voel je je alsof je {gewicht * Jupiter:F2}kg weegt.");
            Console.WriteLine($"op Saturnus voel je je alsof je {gewicht * Saturnus:F2}kg weegt.");
            Console.WriteLine($"op Uranus voel je je alsof je {gewicht * Uranus:F2}kg weegt.");
            Console.WriteLine($"op Neptunus voel je je alsof je {gewicht * Neptunus:F2}kg weegt.");
            Console.WriteLine($"op Pluto voel je je alsof je {gewicht * Pluto:F2}kg weegt.");
        }
        static public void BerekenBtw()
        {
            double bedrag;
            double btw;
            Console.Write("Geef het bedrag in: ");
            bedrag = Convert.ToInt32(Console.ReadLine());
            Console.Write("Geef BTW percentage in: ");
            btw = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"Het bedrag {bedrag} met {btw} btw bedraagt {bedrag * ((btw / 100) + 1):C2}");
        }
        static public void LeetSpeak()
        {
            Console.WriteLine("Geef je tekst in");
            string vervang = Console.ReadLine();
            Console.WriteLine(vervang.Replace(" ","").Replace("a", "@"));
        }
        static public void Instructies()
        {
            Console.WriteLine("Wat is je naam?");
            string naam = Console.ReadLine();
            Console.WriteLine("Wat is de naam van de cusrus?");
            string cursus = Console.ReadLine();
            Console.WriteLine($@"Maak een map al volgt: \{naam.Substring(0,3).ToUpper()}\{cursus}");
        }
        static public void Lotto()
        {
            Console.WriteLine("Wat zijn je cijfers (tussen 01 en 45)?");
            string cijfers = Console.ReadLine();
            Console.WriteLine("Je cijfers zijn:");
            string cijfermettabs = cijfers.Replace(",","\t");
            Console.WriteLine(cijfermettabs.Substring(0, 8));
            Console.WriteLine(cijfermettabs.Substring(9));
        }
        static public void Superlotto()
        {
            Console.WriteLine("Wat zijn je cijfers (tussen 01 en 45)?");
            string cijfers = Console.ReadLine();
            Console.WriteLine("Je cijfers zijn:");
            int komma = cijfers.IndexOf(",");
            int cijfer1 = Convert.ToInt32(cijfers.Substring(0, komma));
            // 2de cijfer
            string tweedecijfer = cijfers.Substring(komma+1);
            komma = tweedecijfer.IndexOf(",");
            int cijfer2 = Convert.ToInt32(tweedecijfer.Substring(0,komma));
            // 3e-5e
            string derdecijfer = tweedecijfer.Substring(komma + 1);
            komma = derdecijfer.IndexOf(",");
            int cijfer3 = Convert.ToInt32(derdecijfer.Substring(0, komma));
            string viercijfer = derdecijfer.Substring(komma + 1);
            komma = viercijfer.IndexOf(",");
            int cijfer4 = Convert.ToInt32(viercijfer.Substring(0, komma));
            string vijfcijfer = viercijfer.Substring(komma + 1);
            komma = vijfcijfer.IndexOf(",");
            int cijfer5 = Convert.ToInt32(vijfcijfer.Substring(0, komma));
            //laatste
            int cijfer6 = Convert.ToInt32(vijfcijfer.Substring(komma + 1));
            //uitvoer
            Console.WriteLine($"{cijfer1:D2}\t{cijfer2:D2}\t{cijfer3:D2}\n{cijfer4:D2}\t{cijfer5:D2}\t{cijfer6:D2}");
        }
    }
}
